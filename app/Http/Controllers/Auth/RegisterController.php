<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Roles;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Monarobase\CountryList\CountryListFacade;
use Monarobase\CountryList\CountryList;




class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'adresse' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
             'phone_number' => 'required|integer|min:8',
            'pays' => ['required', 'string', 'max:255'],
            'ville' => ['required', 'string', 'max:255'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $createUser =  User::create([
            'name' => $data['name'],
            'adresse' => $data['adresse'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'phone_number' => $data['phone_number'],
            'pays' => $data['pays'],
            'ville' => $data['ville'],
        ]);

        //Roles
        /*$roles = Roles::all()->where('role','admin');

        $role = $roles->id;

        $createUser->roles()->attach($role);*/

        $role = Roles::where('role', 'donateur')->first();
        if ($role) {
            $createUser->roles()->attach($role);
        }

        return $createUser;

    }

    public function showRegistrationForm()
    {

        $countries = CountryListFacade::getList('en');
        $roles = Roles::all()->where('role','donateur');

        /*     public function getCitiesForCountry($countryCode)
         {
             $countryList = new CountryList();
             $cities = $countryList->getCitiesForCountry($countryCode);

             return response()->json(['cities' => $cizties]);
         }*/
        /* $countryList = new CountryList();
         $cities = $countryList->getCitiesForCountry($countryCode);*/

        return view('auth.register', compact('countries','roles'));




    }
}
