<?php

namespace App\Http\Controllers;


use App\Models\Roles;
use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Monarobase\CountryList\CountryListFacade;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function listAdmin()
    {

        $admins = User::whereHas('roles', function ($query) {
            $query->where('role', 'admin');
        })->get();



        return view('dashboard.listAdmin', compact('admins'));



    }

    public function listUser()
    {

        $users = User::whereHas('roles', function ($query) {
            $query->where('role', 'donateur');
        })->get();
        return view('dashboard.listUser', compact('users'));
    }

    public function ajoutUser()
    {
        $countries = CountryListFacade::getList('en');
        $roles = Roles::all();
        return view('dashboard.ajoutUser', compact('countries', 'roles'));
    }

}
