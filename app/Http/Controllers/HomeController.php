<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Demandes;
use App\Models\Dons;
use App\Models\Medias;
use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Monarobase\CountryList\CountryListFacade;
use Illuminate\Support\Facades\Http;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {


        /*$request = Request::create("/api/status");
        $response = Route::dispatch($request);

        $status = $response->getData();


        $request = Request::create("/api/categories");
        $response = Route::dispatch($request);

        $categories = $response->getData();*/

        $categories = Categories::all();
        $status = Status::all();

        $dons = Dons::with(['categories', 'medias', 'status'=> function ($query) {
            return $query
                ->where('type', 'pris')
                ->orwhere('type', 'demande');
        }])
            ->limit(9)->get();




       return view('welcome', compact('dons', 'status', 'categories'));
    }

    public function profile()
    {
        //Informations de l'utilisateur connecté
        $countries = CountryListFacade::getList('en');
        $user = Auth::user();
        return view('dashboard.dashboard', compact('user', 'countries'));
    }

    public function listDemandes()
    {

        $demandes = Demandes::where('user_id', auth()->user()->id)->get();



        return view('dashboard.messages', compact('demandes'));
    }

    public function editProfile(Request $request)
    {
        $user = Auth::user()->whereId(Auth::user()->id)->first();
        if ($request) {
            $user->name = $request->nom;
            $user->adresse = $request->adresse;
            $user->email = $request->email;
            $user->phone_number = $request->telephone;
            $user->pays = $request->pays;
            $user->ville = $request->ville;
        }

        $user->update();

        return redirect()->back();

    }

    public function listDons()
    {

        $request = Request::create("/api/status");
        $response = Route::dispatch($request);

        $status = $response->getData();


        $request = Request::create("/api/categories");
        $response = Route::dispatch($request);

        $categories = $response->getData();

        /* $categories = Categories::all();
         $status = Status::all();*/


        $dons = Dons::with(['categories', 'medias', 'status', 'user'])->where('user_id', auth()->user()->id)->get();
        return view('dashboard.listDons',compact('dons'));
    }

    public function formPostDons()
    {
        $categories = Categories::all();

        return view('postDons', compact('categories'));


    }

    public function postDons(Request $request)
    {


            $category_id = $request->categories;
            $status = Status::where('type', 'non_pris')->first();
            $category = Categories::all()->where('id', $category_id)->first();

            //dd($category);
            $user = Auth::user()->whereId(Auth::user()->id)->first();

            /*if($request->nom)
            {
                $nom = $request->nom;
            }

            if($request->description)
            {
                $description = $request->description;
            }*/

            $dons = new Dons;

                $dons->nom = $request->nom;

                $dons->description = $request->description;



            $status->dons()->save($dons);

            $category->dons()->save($dons);

            $user->dons()->save($dons);

            //Enregistrement des images


            $request->validate([
                'files' => 'required',
                'files.*' => 'mimes:jpeg,jpg,png,gif,csv,txt,pdf|max:2048'
            ]);


            foreach ($request->file('files') as $imagefile) {
                $path = $imagefile->store('images_dons', 'public');
                $name = $imagefile->getClientOriginalName();
                $image = new Medias;
                $image->path = $path;
                $image->name = $imagefile->getClientOriginalName();
                $image->dons_id = $dons->id;
                $image->save();
            }

        return redirect()->back();

       // return redirect('postDons')->with('status', 'Image Has been uploaded')->with('image', $name);


    }


    public function  allDonsdashboard(Request $request)
    {
        $request = Request::create("/api/status");
        $response = Route::dispatch($request);

        $status = $response->getData();


        $request = Request::create("/api/categories");
        $response = Route::dispatch($request);

        $categories = $response->getData();

        /* $categories = Categories::all();
         $status = Status::all();*/


        $dons = Dons::with(['categories', 'medias', 'status'])->get();
        return view('dashboard.allDonsdashboard', compact('dons'));
    }


}
