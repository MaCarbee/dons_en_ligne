<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Dons;
use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class IndexController extends Controller
{
    //
    public function index()
    {

        $request = Request::create("/api/status");
        $response = Route::dispatch($request);

        $status = $response->getData();


        $request = Request::create("/api/categories");
        $response = Route::dispatch($request);

        $categories = $response->getData();

        /* $categories = Categories::all();
         $status = Status::all();*/


        $dons = Dons::with(['categories', 'medias', 'status'=> function ($query) {
            return $query
                ->where('type', 'non_pris')
                ->orwhere('type', 'demande');
        }])
            ->limit(9)->get();




     return view('welcome', compact('dons', 'status', 'categories'));
    }
}
