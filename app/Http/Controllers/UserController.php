<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Demandes;
use App\Models\Dons;
use App\Models\Status;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Monarobase\CountryList\CountryListFacade;

class UserController extends Controller
{
    //



    public function contact()
    {
        return view('dashboard.contact');
    }

    public function detailsDons(Request $request)
    {
        $id = $request->id;
        $dons = Dons::find($id);

        $don = Dons::where('id', $id)->with(['categories', 'medias', 'status'])->first();
        $donSimilaires = Dons::where('categorie_id', $dons->categorie_id)->with(['categories', 'medias', 'status'])->get();
        $countries = CountryListFacade::getList('en');

        return view('detailsDons', compact('don', 'donSimilaires', 'countries'));
    }

    public function apropos()
    {
        return view('apropos');
    }

    public function allDons()
    {

         $categories = Categories::all();
        $status = Status::all();


        $dons = Dons::with(['categories', 'medias', 'status'])->get();

        return view('allDons', compact('categories', 'status', 'dons'));
    }

       public function postDemandes(Request $request)
       {
          /* return Validator::make($request, [
               'name' => ['required', 'string', 'max:255'],
               'adresse' => ['required', 'string', 'max:255'],
               'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
               'password' => ['required', 'string', 'min:8', 'confirmed'],
               'phone_number' => 'required|integer|min:8',
               'pays' => ['required', 'string', 'max:255'],
               'ville' => ['required', 'string', 'max:255'],


           ]);*/

           $don = Dons::where('id', $request->id)->first();
           $user_id = $don->user_id;
           $demandes =  Demandes::create([
               'name' => $request->name,
               'adresse' =>$request->adresse,
               'email' =>$request->email,
               'phone_number' => $request->phone_number,
               'pays' => $request->pays,
               'ville' => $request->ville,
               'motif' =>$request->motif,
       'dons_id' => $request->id,
               "user_id" => $user_id
           ]);



           //Le statut du don sera maintenant demande
;
           $don = Dons::with(['status'])->where('id',$request->id)->first();
           $status = Status::where('type', 'demande')->first();
           $don->status_id = $status->id;

           $don->update();

           return redirect()->back();
       }

}
