<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $name
 * @property string $adresse
 * @property integer $phone_number
 * @property string $pays
 * @property string $ville
 * @property string $email
 * @property string $motif
 * @property integer $dons_id
 * @property string $created_at
 * @property string $updated_at
 */
class Demandes extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'adresse', 'phone_number', 'pays', 'ville', 'email', 'motif', 'dons_id', 'user_id', 'created_at', 'updated_at'];

    public function dons()
    {
        return $this->belongsTo(Dons::class);

    }

    public function user()
    {
        return $this->belongsTo(User::class);

    }

}
