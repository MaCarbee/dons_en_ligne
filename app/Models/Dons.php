<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $categorie_id
 * @property integer $status_id
 * @property integer $user_id
 * @property string $nom
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property StatusDon $statusDon
 * @property CategorieDon $categorieDon
 * @property User $user
 */
class Dons extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['categorie_id', 'status_id', 'user_id', 'nom', 'description', 'created_at', 'updated_at'];

    protected $table='dons';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function categories()
    {
        return $this->belongsTo(Categories::class, 'categorie_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function medias()
    {
        return $this->hasMany(Medias::class);
    }

    public function demandes()
    {
        return $this->hasMany(Demandes::class);
    }


}
