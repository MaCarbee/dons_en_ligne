<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $path
 * @property string $name
 * @property integer $dons_id
 * @property string $created_at
 * @property string $updated_at
 */
class Medias extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['path', 'name', 'dons_id', 'created_at', 'updated_at'];


    public function dons()
    {
        return $this->belongsTo(Dons::class);

    }

}
