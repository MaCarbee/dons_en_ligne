<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $role
 * @property string $created_at
 * @property string $updated_at
 */
class Roles extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['role', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsToMany(User::class);
    }
}
