<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    use HasFactory;

    protected $fillable = ['type', 'created_at', 'updated_at'];

    protected $table='status';

    public function dons()
    {
        return $this->hasMany(Dons::class);

    }

}
