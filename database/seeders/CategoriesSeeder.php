<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table("categories")->insert([
            [
                "nom" => "Matériels"
            ],
            [
                "nom" => "Santé"
            ],
            [
                "nom" => "Finances"
            ],
            [
                "nom" => "Aliments"
            ],
            [
                "nom" => "Vêtements"
            ],
            [
                "nom" => "Education"
            ]
        ]);
    }
}
