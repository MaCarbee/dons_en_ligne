<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MediasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table("medias")->insert([
            [
                "path" => "images_dons/fjZaY6wM9HgMk6fUoiSjXObt1tbbhBbqLwBWgB4Y.png",
                "name" => "bakugo.png",
                "dons_id" => 1,

                "created_at" 	=> "2023-03-13 14:08:36",
                "updated_at" 	=> "2023-03-14 00:11:39"
            ],
        ]);
    }
}
