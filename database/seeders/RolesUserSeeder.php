<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table("roles_user")->insert([
            [
                "user_id" => 1,
                "roles_id" => 1,
            ],
            [
                "user_id" => 1,
                "roles_id" => 2,
            ],
            [
                "user_id" => 2,
                "roles_id" => 1,
            ],
            [
                "user_id" => 3,
                "roles_id" => 2,
            ]
        ]);
    }
}
