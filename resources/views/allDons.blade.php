@extends('layouts.layout')

@section('title',''.config('app.name'). '| Tous les dons ')

@section('css')

    <style>
        .page-loading {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 100%;
            -webkit-transition: all .4s .2s ease-in-out;
            transition: all .4s .2s ease-in-out;
            background-color: #fff;
            opacity: 0;
            visibility: hidden;
            z-index: 9999;
        }

        .page-loading.active {
            opacity: 1;
            visibility: visible;
        }

        .page-loading-inner {
            position: absolute;
            top: 50%;
            left: 0;
            width: 100%;
            text-align: center;
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
            -webkit-transition: opacity .2s ease-in-out;
            transition: opacity .2s ease-in-out;
            opacity: 0;
        }

        .page-loading.active > .page-loading-inner {
            opacity: 1;
        }

        .page-loading-inner > span {
            display: block;
            font-family: 'Inter', sans-serif;
            font-size: 1rem;
            font-weight: normal;
            color: #737491;
        }

        .page-spinner {
            display: inline-block;
            width: 2.75rem;
            height: 2.75rem;
            margin-bottom: .75rem;
            vertical-align: text-bottom;
            border: .15em solid #766df4;
            border-right-color: transparent;
            border-radius: 50%;
            -webkit-animation: spinner .75s linear infinite;
            animation: spinner .75s linear infinite;
        }

        @-webkit-keyframes spinner {
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @keyframes spinner {
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

    </style>
    <!-- Page loading scripts-->
    <script>
        (function () {
            window.onload = function () {
                var preloader = document.querySelector('.page-loading');
                preloader.classList.remove('active');
                setTimeout(function () {
                    preloader.remove();
                }, 2000);
            };
        })();

    </script>
    <!-- Vendor Styles-->
    <link rel="stylesheet" media="screen" href="{{asset('vendor/simplebar/dist/simplebar.min.css')}}"/>
    <link rel="stylesheet" media="screen" href="{{asset('vendor/tiny-slider/dist/tiny-slider.css')}}"/>
    <link rel="stylesheet" media="screen" href="{{asset('vendor/lightgallery.js/dist/css/lightgallery.min.css')}}"/>
    <link rel="stylesheet" media="screen" href="{{asset('vendor/flatpickr/dist/flatpickr.min.css')}}"/>
    <!-- Main Theme Styles + Bootstrap-->
    <link rel="stylesheet" media="screen" href="{{asset('css/theme.min.css')}}">
    <!-- Google Tag Manager-->
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                '../www.googletagmanager.com/gtm5445.html?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-WKV3GT5');
    </script>

@endsection


@section('content')


    <section class="bg-secondary py-5 py-md-6">
        <div class="container mt-4 mb-2 mt-md-2 mb-md-0">
            <h2 class="text-center mb-4 pb-2">Dons</h2>
            <div class="masonry-filterable">

                @foreach($dons as $don)
                <div class="masonry-grid gallery overflow-hidden" data-columns="3">
                    <div class="masonry-grid-item" data-groups="[&quot;{{$don->nom}}&quot;]"><a class=""
                 href="{{route('detailsDons',["id" => $don->id])}}"
                                    data-sub-html="&lt;h6 class=&quot;fs-sm text-light&quot;&gt;Lounge room&lt;/h6&gt;"><img
                                src="{{ sizeof($don->medias) > 0 ?
asset('storage/'.$don->medias[0]->path)
 : asset('') }}" alt="Lounge room"></a></div>

                </div>
                @endforeach


            </div>
        </div>
    </section>


@endsection

@section('js')
    <script src="{{asset('vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('vendor/simplebar/dist/simplebar.min.js')}}"></script>
    <script src="{{asset('vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js')}}"></script>
    <script src="{{asset('vendor/jarallax/dist/jarallax.min.js')}}"></script>
    <script src="{{asset('vendor/tiny-slider/dist/min/tiny-slider.js')}}"></script>
    <script src="{{asset('vendor/lightgallery.js/dist/js/lightgallery.min.js')}}"></script>
    <script src="{{asset('vendor/lg-fullscreen.js/dist/lg-fullscreen.min.js')}}"></script>
    <script src="{{asset('vendor/lg-zoom.js/dist/lg-zoom.min.js')}}"></script>
    <script src="{{asset('vendor/imagesloaded/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{asset('vendor/shufflejs/dist/shuffle.min.js')}}"></script>
    <script src="{{asset('vendor/flatpickr/dist/flatpickr.min.js')}}"></script>
    <!-- Main theme script-->
    <script src="{{asset('js/theme.min.js')}}"></script>

@endsection
