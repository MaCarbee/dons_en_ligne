{{--
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
--}}


{{--   <div class="modal fade" id="modal-signin" tabindex="-1">
       <div class="modal-dialog modal-dialog-centered" role="document">
           <div class="modal-content border-0">
               <div class="view show" id="modal-signin-view">
                   <div class="modal-header border-0 bg-dark px-4">
                       <h4 class="modal-title text-light">S'inscrire</h4>
                       <button class="btn-close btn-close-white" type="button" data-bs-dismiss="modal" aria-label="btn-close "></button>
                   </div>
                   <div class="modal-body px-4">
                       <p class="fs-ms text-muted">S'inscrire to your account using email and password provided during registration.</p>

                           <form method="POST" action="{{ route('login') }}" class="needs-validation" novalidate>
                               @csrf
                           <div class="mb-3">
                               <div class="input-group"><i class="ai-mail position-absolute top-50 start-0 translate-middle-y ms-3"></i>
                                   <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                               </div>
                               @error('email')
                               <span class="invalid-feedback" role="alert">
                                       <strong>{{ $message }}</strong>
                                   </span>
                               @enderror
                           </div>
                           <div class="mb-3">
                               <div class="input-group"><i class="ai-lock position-absolute top-50 start-0 translate-middle-y ms-3"></i>
                                   <div class="password-toggle w-100">
                                       <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                       @error('password')
                                       <span class="invalid-feedback" role="alert">
                                       <strong>{{ $message }}</strong>
                                   </span>
                                       @enderror
                                       <label class="password-toggle-btn" aria-label="Show/hide password">
                                           <input class="password-toggle-check" type="checkbox"><span class="password-toggle-indicator"></span>
                                       </label>
                                   </div>
                               </div>
                           </div>
                           <div class="d-flex justify-content-between align-items-center mb-3 mb-3">
                               <div class="form-check">
                                   <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                   <label class="form-check-label fs-sm" for="keep-signed">Se souvenir de moi</label>
                               </div><a class="nav-link-style fs-ms" href="password-recovery.html">Mot de passe oublié ?</a>
                           </div>
                           <button class="btn btn-primary d-block w-100" type="submit">S'inscrire</button>
                           <p class="fs-sm pt-3 mb-0">N'avez vous pas un compte? <a href='#' class='fw-medium' data-view='#modal-signup-view'>Se Connecter</a></p>
                       </form>
                   </div>
               </div>
               <div class="view" id="modal-signup-view">
                   <div class="modal-header border-0 bg-dark px-4">
                       <h4 class="modal-title text-light">Se Connecter</h4>
                       <button class="btn-close btn-close-white" type="button" data-bs-dismiss="modal" aria-label="btn-close"></button>
                   </div>
                   <div class="modal-body px-4">
                       <p class="fs-ms text-muted">Registration takes less than a minute but gives you full control over your orders.</p>
                       <form class="needs-validation" novalidate>
                           <div class="mb-3">
                               <input class="form-control" type="text" placeholder="Full name" required>
                           </div>
                           <div class="mb-3">
                               <input class="form-control" type="text" placeholder="Email" required>
                           </div>
                           <div class="mb-3 password-toggle">
                               <input class="form-control" type="password" placeholder="Password" required>
                               <label class="password-toggle-btn" aria-label="Show/hide password">
                                   <input class="password-toggle-check" type="checkbox"><span class="password-toggle-indicator"></span>
                               </label>
                           </div>
                           <div class="mb-3 password-toggle">
                               <input class="form-control" type="password" placeholder="Confirm password" required>
                               <label class="password-toggle-btn" aria-label="Show/hide password">
                                   <input class="password-toggle-check" type="checkbox"><span class="password-toggle-indicator"></span>
                               </label>
                           </div>
                           <button class="btn btn-primary d-block w-100" type="submit">Se Connecter</button>
                           <p class="fs-sm pt-3 mb-0">Avez-vous déjà un compte ? <a href='#' class='fw-medium' data-view='#modal-signin-view'>S'inscrire</a></p>
                       </form>
                   </div>
               </div>
               <div class="modal-body text-center px-4 pt-2 pb-4">
                   <hr class="my-0">
                   <p class="fs-sm fw-medium text-heading pt-4">Ou S'inscrire avec</p><a class="btn-social bs-facebook bs-lg mx-1 mb-2" href="#"><i class="ai-facebook"></i></a><a class="btn-social bs-twitter bs-lg mx-1 mb-2" href="#"><i class="ai-twitter"></i></a><a class="btn-social bs-instagram bs-lg mx-1 mb-2" href="#"><i class="ai-instagram"></i></a><a class="btn-social bs-google bs-lg mx-1 mb-2" href="#"><i class="ai-google"></i></a>
               </div>
           </div>
       </div>
   </div>--}}
<!-- Navbar (Floating dark)-->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
@extends('layouts.layout')

@section('content')

    <!-- Page content-->
    <section class="container d-flex justify-content-center align-items-center pt-7 pb-4" style="flex: 1 0 auto;">
        <div class="signin-form mt-3">
            <div class="signin-form-inner">
                <!-- S'inscrire view-->
                <div class="view show" id="signin-view">
                    <h1 class="h2 text-center">Se connecter</h1>
                    <p class="fs-ms text-muted mb-4 text-center">S'inscrire to your account using email and password
                        provided during registration.</p>
                    <form method="POST" action="{{ route('login') }}" class="needs-validation" novalidate>
                        @csrf
                        <div class="input-group mb-3"><i
                                class="ai-mail position-absolute top-50 start-0 translate-middle-y ms-3"></i>
                            {{--  <input class="form-control rounded" type="email"  required>--}}
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                   name="email" value="{{ old('email') }}" required autocomplete="email"
                                   placeholder="Email" autofocus>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="input-group mb-3"><i
                                class="ai-lock position-absolute top-50 start-0 translate-middle-y ms-3"></i>
                            <div class="password-toggle w-100">
                                {{--       <input class="form-control" type="password"  required>--}}
                                <input id="password" type="password"
                                       class="form-control @error('password') is-invalid @enderror" name="password"
                                       placeholder="Mot de passe" required autocomplete="current-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <label class="password-toggle-btn" aria-label="Show/hide password">
                                    <input class="password-toggle-check" type="checkbox"><span
                                        class="password-toggle-indicator"></span>
                                </label>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between align-items-center mb-3 pb-1">
                            <div class="form-check">
                                {{--   <input class="form-check-input" type="checkbox" id="">--}}
                                <input class="form-check-input" type="checkbox" name="remember"
                                       id="keep-signed-2" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="keep-signed-2">Se souvenir de moi</label>
                            </div>


                            @if (Route::has('password.request'))

                                <a class="nav-link-style fs-ms"
                                   href="  {{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>

                                </a>
                            @endif
                        </div>
                        <button class="btn btn-primary d-block w-100" type="submit">Se connecter</button>
                        <p class="fs-sm pt-3 mb-0 text-center">N'avez vous pas un compte ? <a
                                href='{{ route('register') }}' class='fw-medium'
                                data-view='#signup-view'>S'inscrire'</a></p>
                    </form>
                </div>
                <!-- Se Connecter view-->
                {{--<div class="view" id="signup-view">
                    <h1 class="h2 text-center">Se connecter</h1>
                    <p class="fs-ms text-muted mb-4 text-center">Registration takes less than a minute but gives you full control over your orders.</p>
                    <form class="needs-validation" novalidate>
                        <div class="mb-3">
                            <input class="form-control" type="text" placeholder="Full name" required>
                        </div>
                        <div class="mb-3">
                            <input class="form-control" type="text" placeholder="Email" required>
                        </div>
                        <div class="input-group mb-3">
                            <div class="password-toggle w-100">
                                <input class="form-control" type="password" placeholder="Password" required>
                                <label class="password-toggle-btn" aria-label="Show/hide password">
                                    <input class="password-toggle-check" type="checkbox"><span class="password-toggle-indicator"></span>
                                </label>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <div class="password-toggle w-100">
                                <input class="form-control" type="password" placeholder="Confirm password" required>
                                <label class="password-toggle-btn" aria-label="Show/hide password">
                                    <input class="password-toggle-check" type="checkbox"><span class="password-toggle-indicator"></span>
                                </label>
                            </div>
                        </div>
                        <button class="btn btn-primary d-block w-100" type="submit">Se Connecter</button>
                        <p class="fs-sm pt-3 mb-0 text-center">Avez-vous déjà un compte? <a href='#' class='fw-medium' data-view='#signin-view'>S'inscrire</a></p>
                    </form>
                </div>
                <div class="border-top text-center mt-4 pt-4">
                    <p class="fs-sm fw-medium text-heading">Ou S'inscrire avec</p><a class="btn-social bs-facebook bs-outline bs-lg mx-1 mb-2" href="#"><i class="ai-facebook"></i></a><a class="btn-social bs-twitter bs-outline bs-lg mx-1 mb-2" href="#"><i class="ai-twitter"></i></a><a class="btn-social bs-instagram bs-outline bs-lg mx-1 mb-2" href="#"><i class="ai-instagram"></i></a><a class="btn-social bs-google bs-outline bs-lg mx-1 mb-2" href="#"><i class="ai-google"></i></a>
                </div>--}}
            </div>
        </div>
    </section>
@endsection
