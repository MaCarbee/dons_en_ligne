{{--@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-end">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection--}}


@extends('layouts.layout')



@section('content')
    <!-- Page content-->
    <!-- Slanted background-->
    <div class="container py-5 py-sm-6 py-md-7">
        <div class="row justify-content-center pt-4">
            <div class="col-lg-7 col-md-9 col-sm-11">
                <h1 class="h2 pb-3">Forgot your password?</h1>
{{--
                <p class="fs-sm">Change your password in three easy steps. This helps to keep your new password secure.</p>
--}}
               {{-- <ul class="list-unstyled fs-sm pb-1 mb-4">
                    <li><span class="text-primary fw-semibold me-1">1.</span>Fill in your email address below.</li>
                    <li><span class="text-primary fw-semibold me-1">2.</span>We'll email you a temporary code.</li>
                    <li><span class="text-primary fw-semibold me-1">3.</span>Use the code to change your password on our secure website.</li>
                </ul>--}}
                <div class="bg-secondary rounded-3 px-3 py-4 p-sm-4">

                        <form method="POST" action="{{ route('password.update') }}" class="needs-validation p-2" novalidate>
                            @csrf
                            <input type="hidden" name="token" value="{{ $token }}">
                        <div class="mb-3 pb-1">
                            <label class="form-label" for="recovery-email">Enter your email address</label>
                            <input class="form-control" type="email" required id="recovery-email">
                            <div class="invalid-feedback">Please provide a valid email address!</div>
                        </div>
                        <button class="btn btn-primary" type="submit">Get new password</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imageUpload").change(function () {
            readURL(this);
        });
    </script>
@endsection
