{{--
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-end">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">

                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
--}}

@extends('layouts.layout')

@section('content')

    <!-- Page content-->
    <div class="container py-5 py-md-7">
        <div class="row align-items-center pt-2">

            <div class="col-md-12 offset-lg-1">
                <h2 class="h3">No account? Sign up</h2>
                <p class="fs-ms text-muted">Registration takes less than a minute but gives you full control over your
                    orders.</p>


                <form method="POST" action="{{ route('register') }}" class="row needs-validation" novalidate>
                    @csrf


                    <div class="col-sm-6 mb-3">
                        <label class="form-label" for="reg-fn">Nom<sup class="text-danger ms-1">*</sup></label>
                        {{--<label for="name" class="col-md-4 col-form-label text-md-end"></label>--}}

                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                               name="name" value="{{ old('name') }}" autocomplete="name" autofocus required id="reg-fn">

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                        <div class="invalid-feedback">Entrez votre nom!</div>
                    </div>

                    <div class="col-sm-6 mb-3">
                        <label class="form-label" for="reg-ln">Adresse<sup class="text-danger ms-1">*</sup></label>

                        <input id="reg-ln" type="text" class="form-control @error('adresse') is-invalid @enderror"
                               name="adresse" value="{{ old('adresse') }}" autocomplete="adresse" autofocus required
                               id="reg-fn">

                        @error('adresse')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                        <div class="invalid-feedback">Entrez votre adresse!</div>
                    </div>

                    <div class="col-sm-6 mb-3">
                        <label class="form-label" for="reg-email">Email address<sup
                                class="text-danger ms-1">*</sup></label>

                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                               name="email" value="{{ old('email') }}" required id="reg-email" autocomplete="email">

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                        <div class="invalid-feedback">Entrez un email valide!</div>
                    </div>

                    <div class="col-sm-6 mb-3">
                        <label class="form-label" for="reg-phone">Phone number</label>
                        <input class="form-control bg-image-0 @error('phone_number') is-invalid @enderror" type="number"
                               id="reg-phone" name="phone_number" required id="reg-phone" autocomplete="new-password">

                        @error('phone_number')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                        <div class="invalid-feedback">Please enter a valid phone number!</div>

                    </div>

                    <div class="col-sm-6 mb-3">
                        <label class="form-label" for="reg-fn">Pays<sup class="text-danger ms-1">*</sup></label>
                        {{--<label for="name" class="col-md-4 col-form-label text-md-end"></label>--}}

                      {{--  <input id="name" type="text" class="form-control @error('pays') is-invalid @enderror"
                               name="pays" value="{{ old('pays') }}" autocomplete="pays" autofocus required id="reg-fn">--}}

                        <select class="form-select" id="reg-fn"  class="form-control @error('pays') is-invalid @enderror"
                                name="pays" value="{{ old('pays') }}" autocomplete="pays" autofocus required id="reg-fn">
                            <option value="">Select Country</option>
                            @foreach ($countries as $country)
                                <option value="{{ $country }}">{{ $country }}</option>
                            @endforeach

                        </select>

                        @error('pays')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                        <div class="invalid-feedback">Entrez votre pays!</div>
                    </div>

                    <div class="col-sm-6 mb-3">
                        <label class="form-label" for="reg-fn">Ville<sup class="text-danger ms-1">*</sup></label>
                        {{--<label for="name" class="col-md-4 col-form-label text-md-end"></label>--}}

                        <input id="name" type="text" class="form-control @error('ville') is-invalid @enderror"
                               name="ville" value="{{ old('ville') }}" autocomplete="ville" autofocus required
                               id="reg-fn">

                        @error('ville')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                        <div class="invalid-feedback">Entrez votre ville!</div>
                    </div>

                    <div class="col-sm-6 mb-3">
                        <label class="form-label" for="reg-password">Password<sup
                                class="text-danger ms-1">*</sup></label>

                        <input id="password" type="password"
                               class="form-control @error('password') is-invalid @enderror" name="password" required
                               id="reg-password" autocomplete="new-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                        <div class="invalid-feedback">Please provide password!</div>
                    </div>
                    <div class="col-sm-6 mb-3">
                        <label class="form-label" for="reg-confirm-password">Confirm password<sup
                                class="text-danger ms-1">*</sup></label>

                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                               required id="reg-confirm-password" autocomplete="new-password">

                        <div class="invalid-feedback">Password doesn't match!</div>
                    </div>
                    <div class="col-sm-3 pt-2 " style="margin: auto">
                        <button class="btn btn-primary d-block w-100" type="submit">Sign up</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
