@extends('layouts.layout')



@section('content')
    <!-- Page content-->
    <!-- Slanted background-->
    <div class="position-relative bg-gradient" style="height: 480px;">
        <div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3000 260">
                <polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
            </svg>
        </div>
    </div>
    <!-- Page content-->
    <div class="container position-relative zindex-5 pb-4 mb-md-3" style="margin-top: -350px;">
        <div class="row">
            <!-- Sidebar-->
        @include('partials.dashboardSidebar')
        <!-- Content-->
            <div class="col-lg-8">
                <div class="d-flex flex-column h-100 bg-light rounded-3 shadow-lg p-4">
                    <div class="py-2 p-md-3">
                        <!-- Title + Delete link-->
                        <div
                            class="d-sm-flex align-items-center justify-content-between pb-4 text-center text-sm-start">
                            <h1 class="h3 mb-2 text-nowrap">Formulaire d'ajout</h1>{{--<a
                                class="btn btn-link text-danger fw-medium btn-sm mb-2" href="#"><i
                                    class="ai-trash-2 fs-base me-2"></i>Delete account </a>--}}
                        </div>
                        <!-- Content-->
                        <div class="bg-secondary rounded-3 p-4 mb-4">
                            <div class="d-block d-sm-flex align-items-center" id="imagePreview"><img
                                    class="d-block rounded-circle mx-sm-0 mx-auto mb-3 mb-sm-0"
                                    src="img/demo/presentation/icons/user.svg" alt="" width="110">
                                <div class="ps-sm-3 text-center text-sm-start">
                                    {{-- <button class="btn btn-light shadow btn-sm mb-2"  type='file' id="imageUpload" name="cover" accept=".png, .jpg, .jpeg"><i
                                             class="ai-refresh-cw me-2"></i>Change avatar
                                     </button>--}}
                                    {{--<div class="p mb-0 fs-ms text-muted">Upload JPG, GIF or PNG image. 300 x 300
                                        required.
                                    </div>--}}
                                </div>
                            </div>
                        </div>
                        <form method="POST" action="{{ route('enregistrement') }}">
                            @csrf
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="mb-3 pb-1">
                                        <label class="form-label px-0" for="account-fn">Nom</label>
                                        <input class="form-control" type="text" id="account-fn" value=""
                                               name="nom">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="mb-3 pb-1">
                                        <label class="form-label px-0" for="account-ln">Adresse</label>
                                        <input class="form-control" type="text" id="account-ln"
                                               value="" name="adresse">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="mb-3 pb-1">
                                        <label class="form-label px-0" for="account-email">Email</label>
                                        <div class="input-group"><span class="input-group-text">@</span>
                                            <input class="form-control" type="email" id="account-email"
                                                   value="" name="email">
                                        </div>

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="mb-3 pb-1">
                                        <label class="form-label px-0" for="account-telephone">Telephone</label>
                                        <input class="form-control" type="tel" id="account-telephone"
                                               value="" name="telephone">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="mb-3 pb-1">
                                        <label class="form-label px-0" for="account-country">Pays</label>
                                        <select class="form-select" id="account-country" name="pays">

                                            <option value=""></option>
                                            @foreach ($countries as $country)

                                                    <option value="{{ $country }}">{{ $country }}</option>

                                            @endforeach

                                        </select>
                                        {{--<input class="form-control" type="text" id="account-country" value="Benin" name="pays">--}}
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="mb-3 pb-1">
                                        <label class="form-label px-0" for="account-city">Ville</label>
                                        <input class="form-control" type="text" id="account-city"
                                               value="" name="ville">
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="mb-3 pb-1">
                                        <label class="form-label px-0" for="account-city">Rôles</label>
                                        <select class="form-select" id="account-country" name="pays">

                                            <option value=""></option>
                                            @foreach ($roles as $role)

                                                <option value="{{ $role->id }}">{{ $role->role }}</option>

                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                {{--<div class="col-sm-6">
                                    <div class="mb-3 pb-1">
                                        <label class="form-label px-0" for="account-address">Mot de passe</label>
                                        <input class="form-control" type="text" id="account-address" name="password"
                                               value="Some Cool Street, 22/1">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="mb-3 pb-1">
                                        <label class="form-label px-0" for="account-zip">Confirmez mot de passe</label>
                                        <input class="form-control" type="text" id="account-zip" name="confirm_passsword">
                                    </div>--}}
                            </div>
                            <div class="col-12">
                                <hr class="mt-2 mb-4">

                                <div class="d-flex flex-wrap justify-content-between align-items-center">
                                    {{--   <div class="form-check d-block">
                                           <input class="form-check-input" type="checkbox" name="showEmail" id="show-email" checked>
                                           <label class="form-check-label" for="show-email">Show my email to registered
                                               users</label>
                                       </div>--}}

                                    <button class="btn btn-primary mt-3 mt-sm-0" type="submit"><i
                                            class="ai-save fs-lg me-2"></i>Enregistrer
                                    </button>
                                </div>


                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection

@section('js')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imageUpload").change(function () {
            readURL(this);
        });
    </script>
@endsection
