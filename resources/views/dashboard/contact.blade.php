
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from around.createx.studio/contacts-v2.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Feb 2021 16:23:52 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <title>KEARP | Contacts </title>
    <!-- SEO Meta Tags-->
    <meta name="description" content="Around - Multipurpose Bootstrap Template">
    <meta name="keywords" content="bootstrap, business, consulting, coworking space, services, creative agency, dashboard, e-commerce, mobile app showcase, multipurpose, product landing, shop, software, ui kit, web studio, landing, html5, css3, javascript, gallery, slider, touch, creative">
    <meta name="author" content="Createx Studio">
    <!-- Viewport-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon and Touch Icons-->
    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
    <link rel="manifest" href="site.webmanifest">
    <link rel="mask-icon" color="#5bbad5" href="safari-pinned-tab.svg">
    <meta name="msapplication-TileColor" content="#766df4">
    <meta name="theme-color" content="#ffffff">
    <!-- Page loading styles-->
    <style>
        .page-loading {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 100%;
            -webkit-transition: all .4s .2s ease-in-out;
            transition: all .4s .2s ease-in-out;
            background-color: #fff;
            opacity: 0;
            visibility: hidden;
            z-index: 9999;
        }
        .page-loading.active {
            opacity: 1;
            visibility: visible;
        }
        .page-loading-inner {
            position: absolute;
            top: 50%;
            left: 0;
            width: 100%;
            text-align: center;
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
            -webkit-transition: opacity .2s ease-in-out;
            transition: opacity .2s ease-in-out;
            opacity: 0;
        }
        .page-loading.active > .page-loading-inner {
            opacity: 1;
        }
        .page-loading-inner > span {
            display: block;
            font-family: 'Inter', sans-serif;
            font-size: 1rem;
            font-weight: normal;
            color: #737491;
        }
        .page-spinner {
            display: inline-block;
            width: 2.75rem;
            height: 2.75rem;
            margin-bottom: .75rem;
            vertical-align: text-bottom;
            border: .15em solid #766df4;
            border-right-color: transparent;
            border-radius: 50%;
            -webkit-animation: spinner .75s linear infinite;
            animation: spinner .75s linear infinite;
        }
        @-webkit-keyframes spinner {
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @keyframes spinner {
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

    </style>
    <!-- Page loading scripts-->
    <script>
        (function () {
            window.onload = function () {
                var preloader = document.querySelector('.page-loading');
                preloader.classList.remove('active');
                setTimeout(function () {
                    preloader.remove();
                }, 2000);
            };
        })();

    </script>
    <!-- Vendor Styles-->
    <link rel="stylesheet" media="screen" href="{{asset('vendor/simplebar/dist/simplebar.min.css')}}"/>
    <link rel="stylesheet" media="screen" href="{{asset('vendor/lightgallery.js/dist/css/lightgallery.min.css')}}"/>
    <!-- Main Theme Styles + Bootstrap-->
    <link rel="stylesheet" media="screen" href="{{asset('css/theme.min.css')}}">
    <!-- Google Tag Manager-->
    <script>
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            '../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WKV3GT5');
    </script>
</head>
<!-- Body-->
<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
    <iframe src="http://www.googletagmanager.com/ns.html?id=GTM-WKV3GT5" height="0" width="0" style="display: none; visibility: hidden;"></iframe>
</noscript>
<!-- Page loading spinner-->
<div class="page-loading active">
    <div class="page-loading-inner">
        <div class="page-spinner"></div><span>Loading...</span>
    </div>
</div>
<main class="page-wrapper">
    <!-- Sign In Modal-->
    <div class="modal fade" id="modal-signin" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content border-0">
                <div class="view show" id="modal-signin-view">
                    <div class="modal-header border-0 bg-dark px-4">
                        <h4 class="modal-title text-light">Sign in</h4>
                        <button class="btn-close btn-close-white" type="button" data-bs-dismiss="modal" aria-label="btn-close "></button>
                    </div>
                    <div class="modal-body px-4">
                        <p class="fs-ms text-muted">Sign in to your account using email and password provided during registration.</p>
                        <form class="needs-validation" novalidate>
                            <div class="mb-3">
                                <div class="input-group"><i class="ai-mail position-absolute top-50 start-0 translate-middle-y ms-3"></i>
                                    <input class="form-control rounded" type="email" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="input-group"><i class="ai-lock position-absolute top-50 start-0 translate-middle-y ms-3"></i>
                                    <div class="password-toggle w-100">
                                        <input class="form-control" type="password" placeholder="Password" required>
                                        <label class="password-toggle-btn" aria-label="Show/hide password">
                                            <input class="password-toggle-check" type="checkbox"><span class="password-toggle-indicator"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-between align-items-center mb-3 mb-3">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="keep-signed">
                                    <label class="form-check-label fs-sm" for="keep-signed">Keep me signed in</label>
                                </div><a class="nav-link-style fs-ms" href="password-recovery.html">Forgot password?</a>
                            </div>
                            <button class="btn btn-primary d-block w-100" type="submit">Sign in</button>
                            <p class="fs-sm pt-3 mb-0">Don't have an account? <a href='#' class='fw-medium' data-view='#modal-signup-view'>Sign up</a></p>
                        </form>
                    </div>
                </div>
                <div class="view" id="modal-signup-view">
                    <div class="modal-header border-0 bg-dark px-4">
                        <h4 class="modal-title text-light">Sign up</h4>
                        <button class="btn-close btn-close-white" type="button" data-bs-dismiss="modal" aria-label="btn-close"></button>
                    </div>
                    <div class="modal-body px-4">
                        <p class="fs-ms text-muted">Registration takes less than a minute but gives you full control over your orders.</p>
                        <form class="needs-validation" novalidate>
                            <div class="mb-3">
                                <input class="form-control" type="text" placeholder="Full name" required>
                            </div>
                            <div class="mb-3">
                                <input class="form-control" type="text" placeholder="Email" required>
                            </div>
                            <div class="mb-3 password-toggle">
                                <input class="form-control" type="password" placeholder="Password" required>
                                <label class="password-toggle-btn" aria-label="Show/hide password">
                                    <input class="password-toggle-check" type="checkbox"><span class="password-toggle-indicator"></span>
                                </label>
                            </div>
                            <div class="mb-3 password-toggle">
                                <input class="form-control" type="password" placeholder="Confirm password" required>
                                <label class="password-toggle-btn" aria-label="Show/hide password">
                                    <input class="password-toggle-check" type="checkbox"><span class="password-toggle-indicator"></span>
                                </label>
                            </div>
                            <button class="btn btn-primary d-block w-100" type="submit">Sign up</button>
                            <p class="fs-sm pt-3 mb-0">Already have an account? <a href='#' class='fw-medium' data-view='#modal-signin-view'>Sign in</a></p>
                        </form>
                    </div>
                </div>
                <div class="modal-body text-center px-4 pt-2 pb-4">
                    <hr class="my-0">
                    <p class="fs-sm fw-medium text-heading pt-4">Or sign in with</p><a class="btn-social bs-facebook bs-lg mx-1 mb-2" href="#"><i class="ai-facebook"></i></a><a class="btn-social bs-twitter bs-lg mx-1 mb-2" href="#"><i class="ai-twitter"></i></a><a class="btn-social bs-instagram bs-lg mx-1 mb-2" href="#"><i class="ai-instagram"></i></a><a class="btn-social bs-google bs-lg mx-1 mb-2" href="#"><i class="ai-google"></i></a>
                </div>
            </div>
        </div>
    </div>
    <!-- Navbar (Solid background)-->
    <!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
@include('partials.header')
    <!-- Page content-->
    <!-- Page title-->
    <section class="jarallax bg-gradient pt-5 pb-7 pt-md-7" data-jarallax data-speed="0.25"><span class="position-absolute top-0 start-0 w-100 h-100 bg-gradient opacity-80"></span>
        <div class="jarallax-img" style="background-image: url(img/pages/contacts/page-title-bg.jpg);"></div>
        <div class="container position-relative zindex-5 pt-3 pb-7 pt-md-0">
            <div class="row justify-content-center py-md-5">
                <div class="col-lg-6 col-md-8 text-center pt-2 mb-n3">
                    <h1 class="text-light">Contacts</h1>
                    <p class="text-light">Get in touch with by completing the contact form or call us now. We normally respond within 2 business days.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact cards (details)-->
    <section class="container-fluid bg-overlay-content mb-5 mb-md-6" style="margin-top: -120px;">
        <div class="row">
            <div class="col-lg-3 col-sm-6 mb-grid-gutter">
                <div class="card h-100 border-0 shadow-lg py-4">
                    <div class="card-body text-center"><i class="ai-map-pin mb-4 text-primary" style="font-size: 2.25rem;"></i>
                        <h3 class="h6 mb-2">Address, Location</h3>
                        <p class="fs-sm mb-2">396 Lillian Blvd, Holbrook, NY 11741, USA</p><a class="fancy-link fs-sm" href="#map" data-scroll>See on the map</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 mb-grid-gutter">
                <div class="card h-100 border-0 shadow-lg py-4">
                    <div class="card-body text-center"><i class="ai-clock mb-4 text-danger" style="font-size: 2.25rem;"></i>
                        <h3 class="h6 mb-2">Working hours</h3>
                        <ul class="list-unstyled fs-sm mb-0">
                            <li>Mon - Fri: 9AM - 8PM </li>
                            <li>Sat: 10AM - 5PM</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 mb-grid-gutter">
                <div class="card h-100 border-0 shadow-lg py-4">
                    <div class="card-body text-center"><i class="ai-phone mb-4 text-success" style="font-size: 2.25rem;"></i>
                        <h3 class="h6 mb-2">Phone numbers</h3>
                        <ul class="list-unstyled fs-sm mb-0">
                            <li><span class="me-1">Customer service:</span><a class="nav-link-style" href="tel:+108044357260">+1 (080) 44 357 260</a></li>
                            <li><span class="me-1">Tech support:</span><a class="nav-link-style" href="tel:+100331697720">+1 00 33 169 7720</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 mb-grid-gutter">
                <div class="card h-100 border-0 shadow-lg py-4">
                    <div class="card-body text-center"><i class="ai-mail mb-4 text-warning" style="font-size: 2.25rem;"></i>
                        <h3 class="h6 mb-2">Email addresses</h3>
                        <ul class="list-unstyled fs-sm mb-0">
                            <li><span class="me-1">Customer service:</span><a class="nav-link-style" href="mailto:customer@example.com">customer@example.com</a></li>
                            <li><span class="me-1">Tech support:</span><a class="nav-link-style" href="mailto:support@example.com">support@example.com</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact cards (offices)-->

    <!-- Map + Contact form-->
    <section class="border-top" id="map">
        <div class="row g-0 gallery"><a class="col-lg-6 gallery-item map-popup d-flex flex-columnt justify-content-center align-items-center bg-position-center bg-size-cover py-7 text-center" href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d193595.91476818218!2d-74.11976253858133!3d40.69740344296443!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew%20York%2C%20NY%2C%20USA!5e0!3m2!1sen!2sua!4v1568574342685!5m2!1sen!2sua" data-iframe="true" data-sub-html="&lt;h6 class=&quot;fs-sm text-light&quot;&gt;396 Lillian Blvd, Holbrook, NY 11741&lt;/h6&gt;" style="background-image:url(img/pages/contacts/map02.jpg);"><span class="gallery-caption"><i class="ai-maximize-2 fs-xl mt-n1 me-2"></i>Expand the map</span>
                <div class="d-inline-block py-4 py-md-6"><img src="img/pages/contacts/marker.png" alt="Map marker" width="48"></div></a>
            <div class="col-lg-6 px-3 px-lg-5">
                <div class="pt-5 pb-6 pt-md-6 mx-auto" style="max-width: 720px;">
                    <h2 class="h3 pb-4">Écrivez-nous</h2>
                    <form class="needs-validation row" novalidate>
                        <div class="col-sm-6 mb-3 pb-1">
                            <label class="form-label" for="cont-fn">Full name<sup class="text-danger ms-1">*</sup></label>
                            <input class="form-control" type="text" id="cont-fn" placeholder="John Doe" required>
                            <div class="invalid-feedback">Please enter your full name!</div>
                        </div>
                        <div class="col-sm-6 mb-3 pb-1">
                            <label class="form-label" for="cont-email">Email address<sup class="text-danger ms-1">*</sup></label>
                            <input class="form-control" type="email" id="cont-email" placeholder="j.doe@example.com" required>
                            <div class="invalid-feedback">Please enter a valid email address!</div>
                        </div>
                        <div class="col-sm-6 mb-3 pb-1">
                            <label class="form-label" for="cont-phone">Phone number</label>
                            <input class="form-control bg-image-0" type="text" id="cont-phone" data-format="custom" data-delimiter="-" data-blocks="2 4 2 2" placeholder="00-0000-00-00">
                        </div>
                        <div class="col-sm-6 mb-3 pb-1">
                            <label class="form-label" for="cont-subject">Subject</label>
                            <input class="form-control bg-image-0" type="text" id="cont-subject" placeholder="Title of your message">
                        </div>
                        <div class="col-12 mb-3 pb-1">
                            <label class="form-label" for="cont-message">Message<sup class="text-danger ms-1">*</sup></label>
                            <textarea class="form-control" id="cont-message" rows="4" placeholder="Write your message here" required></textarea>
                            <div class="invalid-feedback">Please write a message!</div>
                        </div>
                        <div class="col-12 pt-2">
                            <button class="btn btn-primary" type="submit">Send Message</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main>
<!-- Footer-->
@include('partials.footer')
<!-- Back to top button--><a class="btn-scroll-top" href="#top" data-scroll data-fixed-element><span class="btn-scroll-top-tooltip text-muted fs-sm me-2">Top</span><i class="btn-scroll-top-icon ai-arrow-up">   </i></a>
<!-- Vendor scrits: js libraries and plugins-->
<script src="{{asset('vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('vendor/simplebar/dist/simplebar.min.js')}}"></script>
<script src="{{asset('vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js')}}"></script>
<script src="{{asset('vendor/lightgallery.js/dist/js/lightgallery.min.js')}}"></script>
<script src="{{asset('vendor/jarallax/dist/jarallax.min.js')}}"></script>
<script src="{{asset('vendor/cleave.js/dist/cleave.min.js')}}"></script>
<!-- Main theme script-->
<script src="{{asset('js/theme.min.js')}}"></script>
</body>

<!-- Mirrored from around.createx.studio/contacts-v2.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Feb 2021 16:23:56 GMT -->
</html>
