<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from around.createx.studio/dashboard-orders.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Feb 2021 16:23:21 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=utf-8"/><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <title>Around | Dashboard - Orders</title>
    <!-- SEO Meta Tags-->
    <meta name="description" content="Around - Multipurpose Bootstrap Template">
    <meta name="keywords"
          content="bootstrap, business, consulting, coworking space, services, creative agency, dashboard, e-commerce, mobile app showcase, multipurpose, product landing, shop, software, ui kit, web studio, landing, html5, css3, javascript, gallery, slider, touch, creative">
    <meta name="author" content="Createx Studio">
    <!-- Viewport-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon and Touch Icons-->
    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
    <link rel="manifest" href="site.webmanifest">
    <link rel="mask-icon" color="#5bbad5" href="safari-pinned-tab.svg">
    <meta name="msapplication-TileColor" content="#766df4">
    <meta name="theme-color" content="#ffffff">
    <!-- Page loading styles-->
    <style>
        .page-loading {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 100%;
            -webkit-transition: all .4s .2s ease-in-out;
            transition: all .4s .2s ease-in-out;
            background-color: #fff;
            opacity: 0;
            visibility: hidden;
            z-index: 9999;
        }

        .page-loading.active {
            opacity: 1;
            visibility: visible;
        }

        .page-loading-inner {
            position: absolute;
            top: 50%;
            left: 0;
            width: 100%;
            text-align: center;
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
            -webkit-transition: opacity .2s ease-in-out;
            transition: opacity .2s ease-in-out;
            opacity: 0;
        }

        .page-loading.active > .page-loading-inner {
            opacity: 1;
        }

        .page-loading-inner > span {
            display: block;
            font-family: 'Inter', sans-serif;
            font-size: 1rem;
            font-weight: normal;
            color: #737491;
        }

        .page-spinner {
            display: inline-block;
            width: 2.75rem;
            height: 2.75rem;
            margin-bottom: .75rem;
            vertical-align: text-bottom;
            border: .15em solid #766df4;
            border-right-color: transparent;
            border-radius: 50%;
            -webkit-animation: spinner .75s linear infinite;
            animation: spinner .75s linear infinite;
        }

        @-webkit-keyframes spinner {
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @keyframes spinner {
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

    </style>
    <!-- Page loading scripts-->
    <script>
        (function () {
            window.onload = function () {
                var preloader = document.querySelector('.page-loading');
                preloader.classList.remove('active');
                setTimeout(function () {
                    preloader.remove();
                }, 2000);
            };
        })();

    </script>
    <!-- Vendor Styles-->
    <link rel="stylesheet" media="screen" href="{{asset('vendor/simplebar/dist/simplebar.min.css')}}"/>
    <!-- Main Theme Styles + Bootstrap-->
    <link rel="stylesheet" media="screen" href="{{asset('css/theme.min.css')}}">
    <!-- Google Tag Manager-->
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                '../www.googletagmanager.com/gtm5445.html?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-WKV3GT5');
    </script>
</head>
<!-- Body-->
<body style="background-color:#f7f7fc;">
<!-- Google Tag Manager (noscript)-->
<noscript>
    <iframe src="http://www.googletagmanager.com/ns.html?id=GTM-WKV3GT5" height="0" width="0"
            style="display: none; visibility: hidden;"></iframe>
</noscript>
<!-- Page loading spinner-->
<div class="page-loading active">
    <div class="page-loading-inner">
        <div class="page-spinner"></div>
        <span>Loading...</span>
    </div>
</div>
<main class="page-wrapper">
    <!-- Navbar (Floating light)-->
    <!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
@include('partials.header')
    <!-- Page content-->
    <!-- Slanted background-->
    <div class="position-relative bg-gradient" style="height: 480px;">
        <div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3000 260">
                <polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
            </svg>
        </div>
    </div>
    <!-- Page content-->
    <div class="container position-relative zindex-5 pb-4 mb-md-3" style="margin-top: -350px;">
        <div class="row">
            <!-- Sidebar-->
          @include('partials.dashboardSidebar')
            <!-- Content-->
            <div class="col-lg-8">
                <div class="d-flex flex-column h-100 bg-light rounded-3 shadow-lg p-4">
                    <div class="py-2 p-md-3">
                        <!-- Title + Filters-->
                        <div class="d-sm-flex align-items-center justify-content-between pb-2">
                            <h1 class="h3 mb-3 text-center text-sm-start">Dons</h1>
                            <div class="d-flex align-items-center mb-3">
                                <label class="form-label text-nowrap pe-1 me-2 mb-0">Sort orders</label>
                                <select class="form-select form-select-sm">
                                    <option>All</option>
                                    <option>In progress</option>
                                    <option>Delivered</option>
                                    <option>Canceled</option>
                                </select>
                            </div>
                        </div>
                        <!-- Accordion with orders-->
                        <div class="accordion" id="orders-accordion">
                            <!-- Order-->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="order-header-1">
                                    <button
                                        class="accordion-button no-indicator d-flex flex-wrap align-items-center justify-content-between pe-4"
                                        data-bs-toggle="collapse" data-bs-target="#order-collapse-1"
                                        aria-expanded="true" aria-controls="order-collapse-1">
                                        <div class="fs-sm fw-medium text-nowrap my-1 me-2"><i
                                                class="ai-hash fs-base me-1"></i><span
                                                class="d-inline-block align-middle">Dons</span></div>
                                        <div class="text-nowrap text-body fs-sm fw-normal my-1 me-2">
                                        </div>
                                        <div
                                            class="bg-faded-info text-info fs-xs fw-medium py-1 px-3 rounded-1 my-1 me-2">
                                           Catégorie
                                        </div>
                                        <div class="text-body fs-sm fw-medium my-1">Statuts</div>
                                    </button>
                                </h2>
                                <div class="accordion-collapse collapse show" id="order-collapse-1"
                                     aria-labelledby="order-header-1" data-bs-parent="#orders-accordion">
                                    <div class="accordion-body pt-4 bg-secondary rounded-top-0 rounded-3">
                                        <!-- Item-->
                                        @foreach($dons as $don)
                                            <div class="d-sm-flex justify-content-between mb-3 pb-1">
                                                <div class="order-item d-block d-sm-flex me-sm-3"><a
                                                        class="d-table mx-sm-0 mx-auto flex-shrink-0" href="{{ sizeof($don->medias) > 0 ?
asset('storage/'.$don->medias[0]->path)
 : asset('') }}"><img
                                                            class="d-block rounded" src="{{ sizeof($don->medias) > 0 ?
asset('storage/'.$don->medias[0]->path)
 : asset('') }}"
                                                            alt="Thumbnail" width="105"></a>
                                                    <div class="fs-sm pt-2 ps-sm-3 text-center text-sm-start">
                                                        <h5 class="nav-heading fs-sm mb-2"><a href="#">
                                                            </a></h5>
                                                        <div><h5 class="nav-heading fs-sm mb-2"><a href="#">{{$don->nom}}
                                                                </a></h5></div>
                                                        <div><span class="text-muted me-1"></span></div>
                                                    </div>
                                                </div>
                                                <div class="fs-sm text-center pt-2 me-sm-3">
                                                    <div class="text-muted">{{$don->categories->nom}}</div>

                                                </div>
                                                <div class="fs-sm text-center pt-2">
                                                    <div class="text-muted">{{$don->status->type}}</div>

                                                </div>
                                            </div>

                                        @endforeach
                                    </div>
                                </div>
                            </div>

                        <!-- Pagination-->
                        <nav
                            class="d-md-flex justify-content-between align-items-center text-center text-md-start pt-grid-gutter">
                            <div class="d-md-flex align-items-center w-100"><span class="fs-sm text-muted me-md-3">Showing 5 of 13 orders</span>
                                <div class="progress w-100 my-3 mx-auto mx-md-0" style="max-width: 10rem; height: 4px;">
                                    <div class="progress-bar" role="progressbar" style="width: 38%;" aria-valuenow="38"
                                         aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <button class="btn btn-outline-primary btn-sm" type="button">Load more orders</button>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!-- Footer-->
@include('partials/footer')
<!-- Back to top button--><a class="btn-scroll-top" href="#top" data-scroll data-fixed-element><span
        class="btn-scroll-top-tooltip text-muted fs-sm me-2">Top</span><i class="btn-scroll-top-icon ai-arrow-up"> </i></a>
<!-- Vendor scrits: js libraries and plugins-->
<script src="{{asset('vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('vendor/simplebar/dist/simplebar.min.js')}}"></script>
<script src="{{asset('vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js')}}"></script>
<!-- Main theme script-->
<script src="{{asset('js/theme.min.js')}}"></script>
</body>

<!-- Mirrored from around.createx.studio/dashboard-orders.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Feb 2021 16:23:25 GMT -->
</html>
