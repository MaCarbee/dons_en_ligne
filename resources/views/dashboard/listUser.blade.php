@extends('layouts/layout2')

@section('content')
    <div class="position-relative bg-gradient" style="height: 480px;">
        <div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3000 260">
                <polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
            </svg>
        </div>
    </div>
    <!-- Page content-->
    <div class="container position-relative zindex-5 pb-4 mb-md-3" style="margin-top: -350px;">
        <div class="row">
            <!-- Sidebar-->
        @include('partials.dashboardSidebar')
        <!-- Content-->
            <div class="col-lg-8">
                <div class="d-flex flex-column h-100 bg-light rounded-3 shadow-lg p-4">
                    <div class="py-2 p-md-3">
                        <!-- Title-->
                        <h1 class="h3 mb-3 text-nowrap text-center text-sm-start">Donateurs
                            {{--<span
                                class="d-inline-block align-middle bg-faded-dark fs-ms fw-medium rounded-1 py-1 px-2 ms-2">34</span>--}}
                        </h1>
                        <!-- Follower-->
                        @foreach($users as $user)
                            <div class="d-md-flex align-items-center py-grid-gutter border-bottom">

                                <div class="d-flex align-items-center me-md-4 mb-4 mb-md-0" style="max-width: 264px;"><a
                                        class="d-block" href="#"><img class="d-block rounded-circle"
                                                                      src="{{asset('img/demo/presentation/icons/user.svg')}}"
                                                                      alt="Emili Parker" width="90"></a>
                                    <div class="ps-3">
                                        <h2 class="fs-base nav-heading mb-1"><a href="#">{{$user->name}}</a></h2>
                                        <div class="fs-xs text-muted mb-2 pb-1">{{$user->email}}, {{$user->pays}}</div>

                                    </div>
                                </div>

                            </div>
                    @endforeach
                        <!-- Follower-->

                        <!-- Pagination-->
                        <nav
                            class="d-md-flex justify-content-between align-items-center text-center text-md-start pt-grid-gutter">
                            <div class="d-md-flex align-items-center w-100"><span class="fs-sm text-muted me-md-3">Showing 6 of 34 followers</span>
                                <div class="progress w-100 my-3 mx-auto mx-md-0" style="max-width: 10rem; height: 4px;">
                                    <div class="progress-bar" role="progressbar" style="width: 18%;" aria-valuenow="18"
                                         aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <button class="btn btn-outline-primary btn-sm" type="button">Show more followers</button>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
