@extends('layouts.layout')

@section('content')



    <div class="position-relative bg-gradient" style="height: 480px;">
        <div class="shape shape-bottom shape-slant bg-secondary d-none d-lg-block">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3000 260">
                <polygon fill="currentColor" points="0,257 0,260 3000,260 3000,0"></polygon>
            </svg>
        </div>
    </div>
    <!-- Page content-->
    <div class="container position-relative zindex-5 pb-4 mb-md-3" style="margin-top: -350px;">
        <div class="row">
            <!-- Sidebar-->
        @include('partials.dashboardSidebar')
        <!-- Content-->
            <div class="col-lg-8">
                <div class="d-flex flex-column h-100 bg-light rounded-3 shadow-lg p-4">
                    <div class="py-2 p-md-3">
                        <!-- Title + Compose button-->
                        <div
                            class="d-sm-flex align-items-center justify-content-between pb-2 text-center text-sm-start">
                            <h1 class="h3 mb-3 text-nowrap">Demandes
                            </h1>
                            <div class="mb-3"><a class="btn btn-translucent-primary" href="#message-compose"
                                                 data-bs-toggle="collapse"><i class="ai-plus me-2"></i>Compose</a></div>
                        </div>
                        <!-- Message compose form-->
                        <div class="collapse" id="message-compose">
                            <form class="needs-validation shadow rounded mb-4" novalidate>
                                <div
                                    class="d-flex align-items-center justify-content-between bg-dark rounded-top py-3 px-4">
                                    <h3 class="fs-base text-light mb-0">New message</h3><a
                                        class="btn-close btn-close-white" href="#message-compose"
                                        data-bs-toggle="collapse"></a>
                                </div>
                                <div class="p-4">
                                    <div class="mb-3 pb-1">
                                        <input class="form-control" type="text" placeholder="To" required>
                                        <div class="invalid-feedback">Please provide recipient(s) of your message!</div>
                                    </div>
                                    <div class="mb-3 pb-1">
                                        <input class="form-control bg-image-0" type="text" placeholder="Subject">
                                    </div>
                                    <div class="mb-3 pb-1">
                                        <textarea class="form-control" rows="5" required></textarea>
                                        <div class="invalid-feedback">Please write your message!</div>
                                    </div>
                                    <button class="btn btn-primary" type="submit"><i class="ai-send fs-lg me-2"></i>Send
                                    </button>
                                </div>
                            </form>
                        </div>
                        <!-- Toolbar-->
                        <!-- Message list (table)-->
                        <table class="table table-hover border-bottom">
                            <tbody id="message-list">
                            <!-- Message-->
                            @foreach($demandes as $demande)
                            <tr id="item-message-1">
                                <td class="py-3 align-middle ps-2 pe-0" style="width: 2.5rem;">
                                    <div class="form-check ms-2 me-0">

                                        <label class="form-check-label" for="message-1"></label>
                                    </div>
                                </td>
                                <td class="py-3 align-middle"><a
                                        class="d-block d-sm-flex align-items-center text-decoration-none"
                                        href=""><img
                                            class="rounded-circle mb-2 mb-sm-0"
                                            src="{{asset('img/demo/presentation/icons/user.svg')}}"
                                            alt="Edward Johnson" width="42">
                                        <div class="fs-sm ps-sm-3">
                                            <div class="d-sm-flex text-heading align-items-center">
                                                <div class="d-flex align-items-center">
                                                    <div class="text-truncate fw-semibold" style="max-width: 10rem;">
                                                        {{$demande->name}}
                                                    </div>
                                                    <span class="nav-indicator"></span>
                                                </div>
                                                <div class="ms-sm-auto text-muted fs-xs"> {{$demande->created_at}}</div>
                                            </div>
                                            <div class="pt-1 text-heading">{{$demande->motif}}
                                            </div>
                                            <div class="pt-1 text-heading">{{$demande->phone_number}}
                                            </div>
                                            <div class="pt-1 text-heading">{{$demande->adresse}}  {{$demande->ville}}

                                                {{$demande->pays}}                                             </div>
                                        </div>
                                    </a></td>
                            </tr>
                            <!-- Message-->

                            @endforeach
                            </tbody>
                        </table>
                        <!-- Pagination-->
                        <nav
                            class="d-md-flex justify-content-between align-items-center text-center text-md-start pt-3">
                            <div class="d-md-flex align-items-center w-100"><span class="fs-sm text-muted me-md-3">Showing 9 of 84 messages</span>
                                <div class="progress w-100 my-3 mx-auto mx-md-0" style="max-width: 10rem; height: 4px;">
                                    <div class="progress-bar" role="progressbar" style="width: 12%;" aria-valuenow="12"
                                         aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <button class="btn btn-outline-primary btn-sm" type="button">Load older messages</button>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
