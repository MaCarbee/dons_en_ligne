<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from around.createx.studio/portfolio-single-side-gallery-grid.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Feb 2021 16:23:01 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=utf-8"/><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <title>KEARP | DetailsDons </title>
    <!-- SEO Meta Tags-->
    <meta name="description" content="Around - Multipurpose Bootstrap Template">
    <meta name="keywords"
          content="bootstrap, business, consulting, coworking space, services, creative agency, dashboard, e-commerce, mobile app showcase, multipurpose, product landing, shop, software, ui kit, web studio, landing, html5, css3, javascript, gallery, slider, touch, creative">
    <meta name="author" content="Createx Studio">
    <!-- Viewport-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon and Touch Icons-->
    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
    <link rel="manifest" href="site.webmanifest">
    <link rel="mask-icon" color="#5bbad5" href="safari-pinned-tab.svg">
    <meta name="msapplication-TileColor" content="#766df4">
    <meta name="theme-color" content="#ffffff">
    <!-- Page loading styles-->
    <style>
        .page-loading {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 100%;
            -webkit-transition: all .4s .2s ease-in-out;
            transition: all .4s .2s ease-in-out;
            background-color: #fff;
            opacity: 0;
            visibility: hidden;
            z-index: 9999;
        }

        .page-loading.active {
            opacity: 1;
            visibility: visible;
        }

        .page-loading-inner {
            position: absolute;
            top: 50%;
            left: 0;
            width: 100%;
            text-align: center;
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
            -webkit-transition: opacity .2s ease-in-out;
            transition: opacity .2s ease-in-out;
            opacity: 0;
        }

        .page-loading.active > .page-loading-inner {
            opacity: 1;
        }

        .page-loading-inner > span {
            display: block;
            font-family: 'Inter', sans-serif;
            font-size: 1rem;
            font-weight: normal;
            color: #737491;
        }

        .page-spinner {
            display: inline-block;
            width: 2.75rem;
            height: 2.75rem;
            margin-bottom: .75rem;
            vertical-align: text-bottom;
            border: .15em solid #766df4;
            border-right-color: transparent;
            border-radius: 50%;
            -webkit-animation: spinner .75s linear infinite;
            animation: spinner .75s linear infinite;
        }

        @-webkit-keyframes spinner {
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @keyframes spinner {
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

    </style>
    <!-- Page loading scripts-->
    <script>
        (function () {
            window.onload = function () {
                var preloader = document.querySelector('.page-loading');
                preloader.classList.remove('active');
                setTimeout(function () {
                    preloader.remove();
                }, 2000);
            };
        })();

    </script>
    <!-- Vendor Styles-->
    <link rel="stylesheet" media="screen" href="{{asset('vendor/simplebar/dist/simplebar.min.css')}}"/>
    <link rel="stylesheet" media="screen" href="{{asset('vendor/tiny-slider/dist/tiny-slider.css')}}"/>
    <link rel="stylesheet" media="screen" href="{{asset('vendor/lightgallery.js/dist/css/lightgallery.min.css')}}"/>
    <!-- Main Theme Styles + Bootstrap-->
    <link rel="stylesheet" media="screen" href="{{asset('css/theme.min.css')}}">
    <!-- Google Tag Manager-->
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                '../www.googletagmanager.com/gtm5445.html?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-WKV3GT5');
    </script>
</head>
<!-- Body-->
<body>
<!-- Google Tag Manager (noscript)-->
<noscript>
    <iframe src="http://www.googletagmanager.com/ns.html?id=GTM-WKV3GT5" height="0" width="0"
            style="display: none; visibility: hidden;"></iframe>
</noscript>
<!-- Page loading spinner-->
<div class="page-loading active">
    <div class="page-loading-inner">
        <div class="page-spinner"></div>
        <span>Loading...</span>
    </div>
</div>
<main class="page-wrapper">
    <!-- Sign In Modal-->

    <!-- Navbar (Floating dark)-->
    <!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
@include('partials.header')
<!-- Page content-->
    <section class="sidebar-enabled sidebar-end">
        <div class="container">
            <div class="row">
                <!-- Project description-->
                <div class="col-lg-6 sidebar pt-5 pt-lg-7 order-lg-2">
                    <div class="px-sm-4 pt-6 py-lg-6 position-relative zindex-5">
                        <h2 class="h2">{{$don->nom}}</h2>
                        <div class="d-sm-flex align-items-center pt-3 pb-2 mb-5 border-bottom fs-sm">
                            <div class="d-flex align-items-center mb-3">
                                <div class="text-nowrap text-muted me-3"><i
                                        class="ai-calendar me-1"></i><span>{{$don->created_at}}</span>
                                </div>
                                <div class="text-nowrap text-muted"><i
                                        class="ai-tag me-1"></i><span>{{$don->categories->nom}}</span>
                                </div>
                            </div>

                        </div>

                        <h3 class="h5">Description</h3>
                        <p class="fs-sm mb-4 pb-2">{{$don->description}}</p>


                        <h3 class="h5">Share</h3><a class="btn-social bs-outline bs-facebook me-2 my-2" href="#"><i
                                class="ai-facebook"></i></a><a class="btn-social bs-outline bs-twitter me-2 my-2"
                                                               href="#"><i class="ai-twitter"></i></a><a
                            class="btn-social bs-outline bs-google me-2 my-2" href="#"><i class="ai-google"></i></a><a
                            class="btn-social bs-outline bs-pinterest my-2" href="#"><i class="ai-pinterest"></i></a>


                        <a class="btn btn-translucent-primary d-block w-50 mt-4" href="#comment-form"
                           data-bs-toggle="collapse">
                            Faire une demande</a>
                        <!-- Comment form-->
                        <div class="collapse" id="comment-form">
                            <form method="POST" action="{{ route('postDemandes') }}"
                                  class="needs-validation bg-light rounded-3 shadow p-4 p-lg-5 mt-4" novalidate>
                                @csrf

                                <input id="id" type="hidden" class="form-control @error('id') is-invalid @enderror"
                                       name="id" value="{{$don->id}}" required id="reg-id" autocomplete="email">

                                <div class="col-sm-12 mb-3">
                                    <label class="form-label" for="reg-fn">Nom<sup
                                            class="text-danger ms-1">*</sup></label>
                                    {{--<label for="name" class="col-md-4 col-form-label text-md-end"></label>--}}

                                    <input id="name" type="text"
                                           class="form-control @error('name') is-invalid @enderror"
                                           name="name" value="{{ old('name') }}" autocomplete="name" autofocus required
                                           id="reg-fn">

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <div class="invalid-feedback">Entrez votre nom!</div>
                                </div>

                                <div class="col-sm-12 mb-3">
                                    <label class="form-label" for="reg-ln">Adresse<sup class="text-danger ms-1">*</sup></label>

                                    <input id="reg-ln" type="text"
                                           class="form-control @error('adresse') is-invalid @enderror"
                                           name="adresse" value="{{ old('adresse') }}" autocomplete="adresse" autofocus
                                           required
                                           id="reg-fn">

                                    @error('adresse')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <div class="invalid-feedback">Entrez votre adresse!</div>
                                </div>

                                <div class="col-sm-12 mb-3">
                                    <label class="form-label" for="reg-email">Email address<sup
                                            class="text-danger ms-1">*</sup></label>

                                    <input id="email" type="email"
                                           class="form-control @error('email') is-invalid @enderror"
                                           name="email" value="{{ old('email') }}" required id="reg-email"
                                           autocomplete="email">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <div class="invalid-feedback">Entrez un email valide!</div>
                                </div>

                                <div class="col-sm-12 mb-3">
                                    <label class="form-label" for="reg-phone">Phone number</label>
                                    <input class="form-control bg-image-0 @error('phone_number') is-invalid @enderror"
                                           type="number"
                                           id="reg-phone" name="phone_number" required id="reg-phone"
                                           autocomplete="new-password">

                                    @error('phone_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <div class="invalid-feedback">Please enter a valid phone number!</div>

                                </div>

                                <div class="col-sm-12 mb-3">
                                    <label class="form-label" for="reg-fn">Pays<sup
                                            class="text-danger ms-1">*</sup></label>
                                    {{--<label for="name" class="col-md-4 col-form-label text-md-end"></label>--}}

                                    {{--  <input id="name" type="text" class="form-control @error('pays') is-invalid @enderror"
                                             name="pays" value="{{ old('pays') }}" autocomplete="pays" autofocus required id="reg-fn">--}}

                                    <select class="form-select" id="reg-fn"
                                            class="form-control @error('pays') is-invalid @enderror"
                                            name="pays" value="{{ old('pays') }}" autocomplete="pays" autofocus required
                                            id="reg-fn">
                                        <option value="">Select Country</option>
                                        @foreach ($countries as $country)
                                            <option value="{{ $country }}">{{ $country }}</option>
                                        @endforeach

                                    </select>

                                    @error('pays')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <div class="invalid-feedback">Entrez votre pays!</div>
                                </div>

                                <div class="col-sm-12 mb-3">
                                    <label class="form-label" for="reg-fn">Ville<sup
                                            class="text-danger ms-1">*</sup></label>
                                    {{--<label for="name" class="col-md-4 col-form-label text-md-end"></label>--}}

                                    <input id="name" type="text"
                                           class="form-control @error('ville') is-invalid @enderror"
                                           name="ville" value="{{ old('ville') }}" autocomplete="ville" autofocus
                                           required
                                           id="reg-fn">

                                    @error('ville')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <div class="invalid-feedback">Entrez votre ville!</div>
                                </div>

                                <div class="mb-3">
                                    <label class="form-label" for="com-text">Motif<sup class="text-danger ms-1">*</sup></label>
                                    <textarea class="form-control" id="com-text" rows="6"
                                              placeholder="Write your comment here" name="motif" required></textarea>
                                    <div class="invalid-feedback">Please write your comment.</div>
                                    <div class="valid-feedback">Looks good!</div>
                                </div>
                                <button class="btn btn-primary" type="submit">Post comment</button>
                            </form>
                        </div>
                    </div>

                </div>


                <!-- Project gallery (Grid)-->
                <div class="col-lg-6 content pt-2 pt-lg-7 pb-lg-4 order-lg-1" id="gallery">
                    <div class="gallery row pt-5 pb-4 py-lg-5">
                        @foreach($don->medias as $media)
                            <div class="col-sm-6 mb-grid-gutter"><a class="gallery-item rounded-3"
                                                                    href="{{ asset('storage/'.($media->path)) }}">
                                    <img
                                        src="{{ asset('storage/'.($media->path)) }}" alt="{{$media->name}}"></a></div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Related projects (Carousel)-->
    <section class="border-top py-5 py-lg-6">
        <div class="container py-3 py-md-0">
            <h2 class="h3 text-center mb-5">Autres dons de la meme catégorie</h2>
            <div class="tns-carousel-wrapper">
                <div class="tns-carousel-inner"
                     data-carousel-options="{&quot;items&quot;: 2, &quot;controls&quot;: false, &quot;nav&quot;: true, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1, &quot;gutter&quot;: 16},&quot;500&quot;:{&quot;items&quot;:2, &quot;gutter&quot;: 16}, &quot;780&quot;:{&quot;items&quot;:3, &quot;gutter&quot;: 16}, &quot;1000&quot;:{&quot;items&quot;:4, &quot;gutter&quot;: 23}}}">
                    <!-- Item-->
                    @foreach($donSimilaires as $item)
                        <div class="pb-2">
                            <div class="card card-curved-body shadow card-slide mx-1">
                                <div class="card-slide-inner"><img class="card-img" src="{{ sizeof($item->medias) > 0 ?
asset('storage/'.$item->medias[0]->path)
 : asset('') }}"
                                                                   alt="{{$item->name}}"><a
                                        class="card-body text-center"
                                        href="#">
                                        <h3 class="h5 nav-heading mt-1 mb-2">{{$item->nom}}</h3>
                                        <p class="fs-sm text-muted mb-1">{{$item->categories->nom}}</p></a></div>
                            </div>
                        </div>
                        <!-- Item-->
                    @endforeach
                </div>
            </div>
        </div>
    </section>

</main>
<!-- Footer-->
@include('partials/footer')
<!-- Back to top button--><a class="btn-scroll-top" href="#top" data-scroll data-fixed-element><span
        class="btn-scroll-top-tooltip text-muted fs-sm me-2">Top</span><i class="btn-scroll-top-icon ai-arrow-up"> </i></a>
<!-- Vendor scrits: js libraries and plugins-->
<script src="{{asset('vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('vendor/simplebar/dist/simplebar.min.js')}}"></script>
<script src="{{asset('vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js')}}"></script>
<script src="{{asset('vendor/tiny-slider/dist/min/tiny-slider.js')}}"></script>
<script src="{{asset('vendor/lightgallery.js/dist/js/lightgallery.min.js')}}"></script>
<script src="{{asset('vendor/lg-fullscreen.js/dist/lg-fullscreen.min.js')}}"></script>
<script src="{{asset('vendor/lg-zoom.js/dist/lg-zoom.min.js')}}"></script>
<!-- Main theme script-->
<script src="{{asset('js/theme.min.js')}}"></script>
</body>

<!-- Mirrored from around.createx.studio/portfolio-single-side-gallery-grid.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Feb 2021 16:23:07 GMT -->
</html>
