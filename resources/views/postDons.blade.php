@extends('layouts.layout')

@section('title',''.config('app.name'). '| PostDons')

@section('css')
    <style>
        input[type="file"] {
            display: block;
        }
        .imageThumb {
            max-height: 75px;
            border: 2px solid;
            padding: 1px;
            cursor: pointer;
        }
        .pip {
            display: inline-block;
            margin: 10px 10px 0 0;
        }
        .remove {
            display: block;
            background: #444;
            border: 1px solid black;
            color: white;
            text-align: center;
            cursor: pointer;
        }
        .remove:hover {
            background: white;
            color: black;
        }
    </style>

@endsection


@section('js')
    <script>
        $(document).ready(function() {
            if (window.File && window.FileList && window.FileReader) {
                $("#files").on("change", function(e) {
                    var files = e.target.files,
                        filesLength = files.length;
                    for (var i = 0; i < filesLength; i++) {
                        var f = files[i]
                        var fileReader = new FileReader();
                        fileReader.onload = (function(e) {
                            var file = e.target;
                            $("<span class=\"pip\">" +
                                "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
                                "<br/><span class=\"remove\">Remove image</span>" +
                                "</span>").insertAfter("#files");
                            $(".remove").click(function(){
                                $(this).parent(".pip").remove();
                            });

                            // Old code here
                            /*$("<img></img>", {
                              class: "imageThumb",
                              src: e.target.result,
                              title: file.name + " | Click to remove"
                            }).insertAfter("#files").click(function(){$(this).remove();});*/

                        });
                        fileReader.readAsDataURL(f);
                    }
                    console.log(files);
                });
            } else {
                alert("Your browser doesn't support to File API")
            }
        });
    </script>





@endsection

@section('content')

    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
        <img src="images/{{ Session::get('image') }}">
    @endif

    <form method="POST" action="{{ route('postDons') }}" class="sidebar-enabled sidebar-end needs-validation" enctype="multipart/form-data">

        @csrf
        <div class="container">
            <div class="row">
                <!-- Content-->
                <div class="col-lg-6 content py-4">

                    <h1 class="mb-3 pb-4">Publication</h1>





                    <div class="row mb-4">
                        <div class="col-sm-12 mb-3 pb-1">
                            <label class="form-label" for="ch-fn">Nom</label>
                            <input class="form-control" type="text" name="nom" id="ch-fn" required>
                        </div>


                        <div class="col-12 mb-3 pb-1">
                            <label class="form-label" for="ch-country">Catégories</label>
                            <select class="form-select" id="ch-country" name="categories" required>
                                <option value="" selected disabled hidden>Choisir une catégorie</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->nom}}</option>
                                    @endforeach


                            </select>
                        </div>


                        <div class="col-12 mb-3 pb-1">
                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
                            <div class="field" align="left">
                                <label class="form-label" for="ch-order-notes">Uploader vos images</label>

                                <input type="file" id="files" name="files[]" @error('image') is-invalid @enderror multiple />

                                @error('image')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                    </div>



                    <div class="mb-3 pb-1 pb-3 pb-lg-5">
                        <label class="form-label" for="ch-order-notes">Description</label>
                        <textarea class="form-control" id="ch-order-notes" rows="5"
                                  placeholder="Ecrivez ici une description ..." name="description" required></textarea>
                    </div>
                    <div class="col-sm-3 pt-2 " style="margin: auto">
                        <button class="btn btn-primary d-block w-100"  type="submit">Publier</button>
                    </div>

                </div>
                <!-- Sidebar-->
                <div class="col-lg-6 sidebar bg-secondary pt-5 ps-lg-4 pb-md-2">
Image
                </div>
            </div>
        </div>

    </form>



@endsection

