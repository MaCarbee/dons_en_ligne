@extends('layouts.layout')


@section('title',''.config('app.name'). '| Accueil ')



@section('css')
    <link rel="stylesheet" media="screen" href="{{asset('vendor/simplebar/dist/simplebar.min.css')}}"/>
    <link rel="stylesheet" media="screen" href="{{asset('vendor/tiny-slider/dist/tiny-slider.css')}}"/>
    <link rel="stylesheet" media="screen" href="{{asset('vendor/lightgallery.js/dist/css/lightgallery.min.css')}}"/>

    <!-- Main Theme Styles + Bootstrap-->
    <link rel="stylesheet" media="screen" href="{{asset('css/theme.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('js/revolution-slider/css/rs6.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('js/revolution-slider/extra-rev-slider1.css')}}">
@endsection

@section('content')



    <section class="position-relative bg-secondary pt-5 pt-lg-7 pb-7 overflow-hidden">
        <div class="shape shape-bottom shape-curve bg-body">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3000 185.4">
                <path fill="currentColor"
                      d="M3000,0v185.4H0V0c496.4,115.6,996.4,173.4,1500,173.4S2503.6,115.6,3000,0z"></path>
            </svg>
        </div>
        <!-- Content-->
        <div class="container pt-3 pb-4 pt-lg-0">
            <div class="row align-items-center">
                <div class="col-lg-5 mt-lg-n7 pb-5 mb-sm-3 mb-lg-0 pb-lg-0 text-center text-lg-start">
                    <h1 class="display-4" style="">Faites la différence aujourd'hui avec la charité </h1>
                </div>
                <div class="col-lg-7">
                    <div class="parallax mx-auto" style="max-width: 705px;">
                        <div class="parallax-layer position-relative" data-depth="0.1"><img
                                src="img/demo/creative-agency/parallax/layer01.svg" alt="Layer"></div>
                        <div class="parallax-layer" data-depth="0.35"><img
                                src="img/demo/creative-agency/parallax/layer02.svg" alt="Layer"></div>
                        <div class="parallax-layer" data-depth="0.2"><img
                                src="img/demo/creative-agency/parallax/layer03.svg" alt="Layer"></div>
                        <div class="parallax-layer" data-depth="0.5"><img
                                src="img/demo/creative-agency/parallax/layer04.svg" alt="Layer"></div>
                        <div class="parallax-layer" data-depth="0.6"><img
                                src="img/demo/creative-agency/parallax/layer05.svg" alt="Layer"></div>
                        <div class="parallax-layer" data-depth="0.4"><img
                                src="img/demo/creative-agency/parallax/layer06.svg" alt="Layer"></div>
                        <div class="parallax-layer" data-depth="0.25"><img
                                src="img/demo/creative-agency/parallax/layer07.svg" alt="Layer"></div>
                        <div class="parallax-layer" data-depth="0.35"><img
                                src="img/demo/creative-agency/parallax/layer08.svg" alt="Layer"></div>
                        <div class="parallax-layer" data-depth="0.6"><img
                                src="img/demo/creative-agency/parallax/layer09.svg" alt="Layer"></div>
                        <div class="parallax-layer" data-depth="0.45"><img
                                src="img/demo/creative-agency/parallax/layer10.svg" alt="Layer"></div>
                        <div class="parallax-layer" data-depth="0.7"><img
                                src="img/demo/creative-agency/parallax/layer11.svg" alt="Layer"></div>
                        <div class="parallax-layer" data-depth="0.3"><img
                                src="img/demo/creative-agency/parallax/layer12.svg" alt="Layer"></div>
                        <div class="parallax-layer" data-depth="0.15"><img
                                src="img/demo/creative-agency/parallax/layer13.png" alt="Layer"></div>
                        <div class="parallax-layer" data-depth="0.9"><img
                                src="img/demo/creative-agency/parallax/layer14.svg" alt="Layer"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container pt-5 pb-4 py-md-6 pt-lg-7">
        <h2 class="text-center pt-3">Services</h2>
        <p class="d-block fs-sm text-muted text-center mx-auto pb-4 mb-5" style="max-width: 400px;">Lorem ipsum dolor
            sit amet, consectetur adipisicing elit. Veritatis eum modi, adipisci facilis.</p>
        <div class="row">

            <div
                class="col-lg-3 offset-lg-1 col-sm-4 col-6 mb-grid-gutter bg-light shadow-lg rounded-3 p-4 mb-grid-gutter text-center text-sm-start">
                <div class="icon-box"><img src="{{asset('img/images/icon-1.png')}}" alt=""></div>
                <h3 class="h5 mt-3 ">Devenez bénévole</h3>
                <p class="fs-sm">Find aute irure dolor in reprehend in voluptate velit esse cillum dolore eu fugiat
                    nulla pariatur. </p>

            </div>
            <div
                class="col-lg-3 offset-lg-1 col-sm-4 col-6 mb-grid-gutter bg-light shadow-lg rounded-3 p-4 mb-grid-gutter text-center text-sm-start">
                <div class="icon-box"><img src="{{asset('img/images/icon-2.png')}}" alt=""></div>
                <h3 class="h5 mt-3">Envoyez-nous des dons
                </h3>
                <p class="fs-sm">Find aute irure dolor in reprehend in voluptate velit esse cillum dolore eu fugiat
                    nulla pariatur. </p>
            </div>
            <div
                class="col-lg-3 offset-lg-1 col-sm-4 col-6 mb-grid-gutter bg-light shadow-lg rounded-3 p-4 mb-grid-gutter text-center text-sm-start">
                <div class="icon-box"><img src="{{asset('img/images/icon-3.png')}}" alt=""></div>
                <h3 class="h5 mt-3">Obtenez de l’aide directement</h3>
                <p class="fs-sm">Find aute irure dolor in reprehend in voluptate velit esse cillum dolore eu fugiat
                    nulla pariatur. </p>
            </div>


        </div>
    </section>

    <section class="bg-secondary">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-4 mt-3 mt-md-0 py-5 py-md-0">
                    <h2 class="text-lg-nowrap pb-2 text-center text-md-start">Notre mission est de changer le monde</h2>
                    <p class="pb-2 text-center text-md-start">Our values ind aute irure dolor in reprehenderit in
                        voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint.</p>
                    <ul class="list-unstyled fs-sm pb-1 d-table mx-auto mx-md-0">
                        <li class="d-flex mb-2"><i
                                class="ai-check-circle text-success fs-lg me-3"></i><span>Education</span>
                        </li>
                        <li class="d-flex mb-2"><i
                                class="ai-check-circle text-success fs-lg me-3"></i><span>Sourire</span>
                        </li>
                        <li class="d-flex mb-2"><i
                                class="ai-check-circle text-success fs-lg me-3"></i><span>Charité</span>
                        </li>
                    </ul>

                </div>
                <div class="col-md-8">
                    <div class="parallax ms-auto" style="max-width: 757px;"><img class="d-block"
                                                                                 src="img/demo/marketing-seo/about-img/lines.png"
                                                                                 alt="Lines">
                        <div class="parallax-layer" style="z-index: 2;" data-depth="0.15"><img
                                src="img/demo/marketing-seo/about-img/image.png" alt="Image"></div>
                        <div class="parallax-layer" style="z-index: 3;" data-depth="0.3"></div>
                        <div class="parallax-layer" data-depth="0.25"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>




    <!-- Statistics (Digits)-->

    <!-- Schedule-->
    <!-- Contacts + Gallery-->
    <section class="container pt-5 pt-md-6 pt-lg-7">
        <h2 class="text-center pt-3 pt-md-2 pb-4">Dons</h2>
        <div class="masonry-filterable mb-3">
            <ul class="masonry-filters nav nav-tabs justify-content-center mt-2 pb-4">

                <li class="nav-item"><a class="nav-link" href="#" data-group="">Tout</a></li>


                @foreach($categories as $category)

                    <li class="nav-item"><a class="nav-link" href="#"
                                            data-group="{{$category->nom}}">{{$category->nom}}</a></li>



                @endforeach
            </ul>

            <div class="masonry-grid" data-columns="4">
                @foreach($dons as $don)
                    <div class="masonry-grid-item" data-groups="[&quot;{{$don->categories->nom}}&quot;]">
                        <div class="card card-flip">
                            <div class="card-flip-inner">
                                <div class="card-flip-front">

                                    <img class="card-img" src="{{ sizeof($don->medias) > 0 ?
asset('storage/'.$don->medias[0]->path)
 : asset('') }}"
                                         alt="">

                                    {{--<img class="card-img" src="{{ asset('storage/'.($don->medias[0]->path ?? 'img/shop/catalog/01.jpg')) }}"
                                         alt="{{$don->nom}}">--}}

                                </div>
                                <a class="card-flip-back" href="{{route('detailsDons',["id"=>$don->id])}}">
                                    <div class="card-body">
                                        <div class="card-body-inner">
                                            <h3 class="h5 card-title mb-2">{{$don->nom}}</h3>
                                            <p class="fs-sm text-muted"> {{$don->categories->nom}}</p><span
                                                class="btn btn-primary btn-sm">Voir plus</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
        <div class="text-center"><a class="btn btn-primary" href="{{route('allDons')}}">Voir plus</a></div>
    </section>

    <section class="container py-4 py-md-6 py-lg-5 ">

        <h2 class="text-center mb-5">Nous croyons que nous pouvons sauver
            Plus de vies avec vous</h2>

        <div class="row align-items-center">
            <div class="col-3">
                <div class="bg-light shadow-lg rounded-3 p-4 mb-grid-gutter text-center text-sm-start"><img
                        class="d-inline-block mb-4 mt-2" src="{{asset('img/images/education.png')}}" alt="Icon"
                        width="100">
                    <h3 class="h5 mb-2">Education</h3>
                    <p class="fs-sm">Find aute irure dolor in reprehend in voluptate velit esse cillum dolore eu fugiat
                        nulla pariatur. </p>
                </div>
            </div>
            <div class="col-3">
                <div class="bg-light shadow-lg rounded-3 p-4 mb-grid-gutter text-center text-sm-start"><img
                        class="d-inline-block mb-4 mt-2" src="{{asset('img/images/food.png')}}" alt="Icon"
                        width="100">
                    <h3 class="h5 mb-2">Alimentation saine</h3>
                    <p class="fs-sm">Find aute irure dolor in reprehend in voluptate velit esse cillum dolore eu fugiat
                        nulla pariatur. </p>
                </div>
            </div>
            <div class="col-3">
                <div class="bg-light shadow-lg rounded-3 p-4 mb-grid-gutter text-center text-sm-start"><img
                        class="d-inline-block mb-4 mt-2" src="{{asset('img/images/hopital.png')}}" alt="Icon"
                        width="100">
                    <h3 class="h5 mb-2">Soins médicaux</h3>
                    <p class="fs-sm">Find aute irure dolor in reprehend in voluptate velit esse cillum dolore eu fugiat
                        nulla pariatur. </p>
                </div>
            </div>
            <div class="col-3">
                <div class="bg-light shadow-lg rounded-3 p-4 mb-grid-gutter text-center text-sm-start"><img
                        class="d-inline-block mb-4 mt-2" src="{{asset('img/images/happy.png')}}" alt="Icon"
                        width="100">
                    <h3 class="h5 mb-2">Sourire</h3>
                    <p class="fs-sm">Find aute irure dolor in reprehend in voluptate velit esse cillum dolore eu fugiat
                        nulla pariatur. </p>
                </div>
            </div>

        </div>
    </section>

@endsection

@section('js')
    <script src="{{asset('vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('vendor/simplebar/dist/simplebar.min.js')}}"></script>
    <script src="{{asset('vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js')}}"></script>
    <script src="{{asset('vendor/jarallax/dist/jarallax.min.js')}}"></script>
    <script src="{{asset('vendor/tiny-slider/dist/min/tiny-slider.js')}}"></script>
    <script src="{{asset('vendor/lightgallery.js/dist/js/lightgallery.min.js')}}"></script>
    <script src="{{asset('vendor/lg-fullscreen.js/dist/lg-fullscreen.min.js')}}"></script>
    <script src="{{asset('vendor/lg-zoom.js/dist/lg-zoom.min.js')}}"></script>
    <script src="{{asset('vendor/imagesloaded/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{asset('vendor/shufflejs/dist/shuffle.min.js')}}"></script>
    <script src="{{asset('vendor/flatpickr/dist/flatpickr.min.js')}}"></script>
    <!-- Main theme script-->
    <script src="{{asset('js/theme.min.js')}}"></script>
    <script src="{{asset('js/revolution-slider/js/revolution.tools.min.js')}}"></script>
    <script src="{{asset('js/revolution-slider/js/rs6.min.js')}}"></script>
    <script src="{{asset('js/revolution-slider/extra-rev-slider1.js')}}"></script>
@endsection
