<?php

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\IndexController;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Accueil
//Route::get('/', function () {
//    return view('welcome');
//});



Route::get('/', [HomeController::class, 'index'])->name('home');

Auth::routes();

/*Route::get('/', function () {
    return view('welcome');
})->name('home');*/



Route::get('/register', 'App\Http\Controllers\Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/register', 'App\Http\Controllers\Auth\RegisterController@register');




//Dashboard

//Profile info

Route::get('/dashboard', 'App\Http\Controllers\HomeController@profile')->name('dashboard');
/*Route::get('/editProfile', [UserController::class, 'editProfile'])->name('editProfile');*/
Route::match(array('GET', 'POST'), 'editProfile', [HomeController::class, 'editProfile'])->name('editProfile');

Route::group(['prefix' => "dashboardlist"], function () {
//Dons
Route::get('/MesDons', [HomeController::class, 'listDons'])->name('mesDons');

//Demandes
Route::get('/demandes', [HomeController::class, 'listDemandes'])->name('demandes');





    Route::middleware(['auth', 'role:admin'])->group(function () {

        //Dashboard Tous les dons
        Route::get('/TousLesDons', [HomeController::class, 'allDonsdashboard'])->name('allDonsdashboard');

    //Liste des admins
    Route::get('/Admin', [Controller::class, 'listAdmin'])->name('listAdmin');

    //Liste des utilisateurs
    Route::get('/User', [Controller::class, 'listUser'])->name('listUser');

    //Ajouter des admins et donateurs
    Route::get('/ajoutUser', [Controller::class, 'ajoutUser'])->name('ajoutUser');

    });



});

Route::group(['prefix' => "list"], function () {
//All dons
Route::get('/TousLesDons', [UserController::class, 'allDons'])->name('allDons');
});

//infoDemandes
Route::get('/infoDemandes', [HomeController::class, 'infoDemandes'])->name('infoDemandes');


//Fin ... dashboard

//Poster les dons
Route::get('/formPostDons', [HomeController::class, 'formPostDons'])->name('formPostDons');
Route::match(array('GET', 'POST'), 'postDons', [HomeController::class, 'postDons'])->name('postDons');

//Poster demandes
Route::match(array('GET', 'POST'), 'postDemandes', [UserController::class, 'postDemandes'])->name('postDemandes');


//Contact
Route::get('/contact', [UserController::class, 'contact'])->name('contact');

//Détails dons
Route::get('/detailsDons/{id}', [UserController::class, 'detailsDons'])->name('detailsDons');

//A propos
Route::get('/apropos', [UserController::class, 'apropos'])->name('apropos');


/*Route::get('/email/verify', function () {
    return view('auth.verify');
})->middleware('auth')->name('verification.notice');

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();

    return redirect('/home');
})->middleware(['auth', 'signed'])->name('verification.verify');



Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();

    return back()->with('message', 'Verification link sent!');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');*/
